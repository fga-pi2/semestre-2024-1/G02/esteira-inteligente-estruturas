# Controle de atividades

## Visão geral do projeto de estruturas:
### Etapa de modelagem:
- [x] Definição dos requisitos iniciais (funcionais e não funcionais);
- [x] Esboço do modelo inicial;
- [x] CAD dos componentes; (27/04)
- [x] Integração em CAD dos componentes; (27/04)
- [x] Integração em CAD dos subsistemas; (29/04)
- [x] Renderização para o relatório; (30/04)
- [x] Desenhos técnicos; (30/04)
- [x] Esquemáticos para o relatório; (30/04)

### Etapa de validação:
- [x] Seleção de pontos críticos de esforços estruturais; (29/04)
- [ ] Validação teórica com cálculos estruturais; (18/05)
- [ ] Análise estrutural numérica (CAE), se necessário; (18/05)
- [ ] Comparação teórico-numérico; (18/05)

### Etapa de construção:
- [x] Listagem de materiais e componentes;
- [x] Orçamento de materiais e componentes;
- [ ] Aquisição dos materiais e componentes;
- [ ] Fabricação dos componentes;
- [ ] Montagem dos subsistemas;
- [ ] Integração completa da estrutura mecânica;

### Etapa de testagem:
- [ ] Integração eletromecânica;
- [ ] Testes iniciais o funcionamento da estrutura;
- [ ] Correção dos componentes necessários;
- [ ] Testagem final;
- [ ] Validação completa da estrutura.


